package com.cybage.AdzHub.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cybage.AdzHub.models.*;
import com.cybage.AdzHub.repository.*;

@Repository
public interface CampaignItemRepository extends JpaRepository<CampaignItem,Integer> {

}
