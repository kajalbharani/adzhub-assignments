package com.cybage.AdzHub.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cybage.AdzHub.models.Creative;

@Repository
public interface CreativeRepository extends JpaRepository<Creative,Integer> {

}
