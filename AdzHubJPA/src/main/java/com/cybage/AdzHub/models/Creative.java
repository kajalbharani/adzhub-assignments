package com.cybage.AdzHub.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "creative_table")
public class Creative {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "creative_id")
	private int creative_id;

	private String creative_name;
	@Enumerated(EnumType.STRING)

	private Types type;

	@ManyToOne
	@JoinColumn(name = "campaign_item_id")
	private CampaignItem campaign_item;

	public int getCreative_id() {
		return creative_id;
	}

	public void setCreative_id(int creative_id) {
		this.creative_id = creative_id;
	}

	public String getCreative_name() {
		return creative_name;
	}

	public void setCreative_name(String creative_name) {
		this.creative_name = creative_name;
	}

	public Types getType() {
		return type;
	}

	public void setType(Types type) {
		this.type = type;
	}

	public CampaignItem getCamapaign_item() {
		return campaign_item;
	}

	public void setCampaign_item(CampaignItem campaign_item) {
		this.campaign_item = campaign_item;
	}

}
