package com.cybage.AdzHub.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.cybage.AdzHub.exceptions.DSPNotAllowedException;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "campaign_item_id")
@Entity
@Table(name="campaign_item_table")
public class CampaignItem {
	
	public CampaignItem() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name= "campaign_item_id")
	private int campaign_item_id;
	
//	public CampaignItem(int campaign_item_id, String campaign_name, Dsp campaign_dsp, List<Creative> creative) {
//		super();
//		this.campaign_item_id = campaign_item_id;
//		this.campaign_name = campaign_name;
//		this.campaign_dsp = campaign_dsp;
//		this.creative = creative;
//	}

	private String campaign_name;
	@Enumerated(EnumType.STRING)
	
	private Dsp campaign_dsp;
	
	@OneToMany(mappedBy = "campaign_item")
	private List<Creative> creative = new ArrayList<Creative>();

	
	public int getCampaign_item_id() {
		return campaign_item_id;
	}

	public void setCampaign_item_id(int campaign_item_id) {
		this.campaign_item_id = campaign_item_id;
	}

	public String getCampaign_name() {
		return campaign_name;
	}

	public void setCampaign_name(String campaign_name) {
		this.campaign_name = campaign_name;
	}

	public Dsp getCampaign_dsp() {
		return campaign_dsp;
	}

	public void setCampaign_dsp(Dsp campaign_dsp) throws DSPNotAllowedException {
		if (!campaign_dsp.equals(Dsp.AppNexus))
			throw new DSPNotAllowedException("Allowed DSP values:MediaMAth/AppNexus/DCP");
		else if (!campaign_dsp.equals(Dsp.MediaMAth))
			throw new DSPNotAllowedException("Allowed DSP values:MediaMAth/AppNexus/DCP");
		else if (!campaign_dsp.equals(Dsp.DCP))
			throw new DSPNotAllowedException("Allowed DSP values:MediaMAth/AppNexus/DCP");
		
		this.campaign_dsp = campaign_dsp;
	}

	public List<Creative> getCreative() {
		return creative;
	}

	public void setCreative(List<Creative> creative) {
		this.creative = creative;
	}
	
	
	

}
