package com.cybage.AdzHub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

//@ComponentScan("com.cybage.AdzHub.models")
@SpringBootApplication
public class AdzHubApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdzHubApplication.class, args);
	}

}

