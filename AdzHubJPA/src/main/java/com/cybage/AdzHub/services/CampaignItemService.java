package com.cybage.AdzHub.services;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cybage.AdzHub.exceptions.CampaignItemNotFoundException;
import com.cybage.AdzHub.exceptions.DSPNotAllowedException;
import com.cybage.AdzHub.models.CampaignItem;
import com.cybage.AdzHub.models.Dsp;
import com.cybage.AdzHub.repository.CampaignItemRepository;

@Service
public class CampaignItemService {

	@Autowired
	CampaignItemRepository campaignItemRepo;

	public CampaignItem findById(Integer id) throws Exception {
		return campaignItemRepo.findById(id)
				.orElseThrow(() -> new CampaignItemNotFoundException("CampaignItem " + id + " not found"));
	}

	public Collection<CampaignItem> findAllCampaignItems() {
		return campaignItemRepo.findAll();
	}

	public CampaignItem createCampaignItem(CampaignItem campaign_item) {
		
		
		return campaignItemRepo.save(campaign_item);
	}

	public void deleteCampaignItem(CampaignItem campaign_item) {

		campaignItemRepo.delete(campaign_item);
	}

}
