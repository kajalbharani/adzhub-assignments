package com.cybage.AdzHub.services;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cybage.AdzHub.exceptions.CampaignItemNotFoundException;
import com.cybage.AdzHub.models.CampaignItem;
import com.cybage.AdzHub.models.Creative;
import com.cybage.AdzHub.repository.CampaignItemRepository;
import com.cybage.AdzHub.repository.CreativeRepository;

@Service
public class CreativeService {
	@Autowired
	CreativeRepository creativeRepo;
	
	@Autowired
	CampaignItemRepository campaignItemRepo;

	public Creative findById(Integer id) throws Exception {
		return creativeRepo.findById(id)
				.orElseThrow(() -> new CampaignItemNotFoundException("Creative " + id + " not found"));
	}

	public Collection<Creative> findAllCreatives() {
		return creativeRepo.findAll();
	}

	public Creative createCreative(Creative creative, Integer id) throws CampaignItemNotFoundException {
		CampaignItem campaign_item = campaignItemRepo.findById(id).orElseThrow(() -> new CampaignItemNotFoundException("CampaignItem " + id + " not found"));
		creative.setCampaign_item(campaign_item);
		return creativeRepo.save(creative);
	}

	public void deleteCreative(Creative creative) {

		creativeRepo.delete(creative);
	}

}
