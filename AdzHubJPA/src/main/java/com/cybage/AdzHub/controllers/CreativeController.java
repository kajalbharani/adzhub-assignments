package com.cybage.AdzHub.controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//imporcom.cybage.AdzHub.models.CampaignItem;
import com.cybage.AdzHub.models.Creative;
//import com.cybage.AdzHub.services.CampaignItemService;
import com.cybage.AdzHub.services.CreativeService;

@RestController
@RequestMapping("/creative")
public class CreativeController {
	
	@Autowired
	CreativeService creativeService;
	
	@GetMapping(path = {"/{creative_id}"})
	public  Creative findCreativeById(@PathVariable("creative_id") int creative_id ) throws Exception{
	    return creativeService.findById(creative_id);
	}
	
	@GetMapping("/get")
	public Collection<Creative> getCreative(){
		return creativeService.findAllCreatives();
		
	}
	
	@PostMapping(path = {"/save"})
	public Creative createCreative(@RequestBody Creative creative, @RequestParam("campaign_item_id") Integer campaign_item_id) throws Exception{
		
		return creativeService.createCreative(creative,campaign_item_id);
	}
	
	
	@DeleteMapping(path = {"/delete"})
	public void deleteCampaignItem(@RequestBody Creative creative){
		creativeService.deleteCreative(creative);
	}
}