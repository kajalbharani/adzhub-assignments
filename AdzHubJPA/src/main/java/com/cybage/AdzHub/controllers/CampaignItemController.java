package com.cybage.AdzHub.controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cybage.AdzHub.exceptions.DSPNotAllowedException;
import com.cybage.AdzHub.models.CampaignItem;
import com.cybage.AdzHub.models.Dsp;
import com.cybage.AdzHub.services.CampaignItemService;
//import com.cybage.AdzHub.exceptions.*;

@RestController
@RequestMapping("/campaign")
public class CampaignItemController {
	
	@Autowired
	CampaignItemService campaignItemService;
	
	@GetMapping(path = {"/{campaign_item_id}"})
	public  CampaignItem findCampaignById(@PathVariable("campaign_item_id") int campaign_item_id ) throws Exception{
	    return campaignItemService.findById(campaign_item_id);
	}
	
	@GetMapping("/get")
	public Collection<CampaignItem> getCmpaignItem(){
		return campaignItemService.findAllCampaignItems();
		
	}
	
	@PostMapping(path = {"/save"})
	
	public CampaignItem createCampaignItem(@RequestBody CampaignItem campaign_item) throws DSPNotAllowedException{
		System.out.println(campaign_item.getCampaign_dsp());

		return campaignItemService.createCampaignItem(campaign_item);
	}
	
	
	@DeleteMapping(path = {"/delete"})
	public void deleteCampaignItem(@RequestBody CampaignItem campaign_item){
		campaignItemService.deleteCampaignItem(campaign_item);
	}
	
	
	
	
	

}
