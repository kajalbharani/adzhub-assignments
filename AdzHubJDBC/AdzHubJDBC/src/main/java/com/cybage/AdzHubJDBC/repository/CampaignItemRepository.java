package com.cybage.AdzHubJDBC.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cybage.AdzHubJDBC.model.CampaignItem;
import com.cybage.AdzHubJDBC.model.CampaignItemRowMapper;


@Transactional
@Repository
public class CampaignItemRepository {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	
	public int addcampaignItem(CampaignItem campaignItem){
		String sql = "Insert into campaign_item_table(campaign_item_id,campaign_name,campaign_dsp) values(?,?,?)";
		return jdbcTemplate.update(sql, campaignItem.getCampaignItemId(), campaignItem.getCampaignName(), campaignItem.getCampaignDsp() );
	}
	
	public CampaignItem getCampaignItemById(int campaignItemId){
		String sql ="Select * from campaign_item_table where campaign_item_id=?";
		RowMapper<CampaignItem> rowMapper = new CampaignItemRowMapper();
		return	jdbcTemplate.queryForObject(sql, rowMapper, campaignItemId);
		
	}
	
	public List<CampaignItem> getAllCampaigns(){
		String sql = "Select * from campaign_item_table";
		RowMapper<CampaignItem> rowMapper = new CampaignItemRowMapper();
		return jdbcTemplate.query(sql, rowMapper);
		
		
	}
	public void deleteCampaignItem(int campaignItemId){
		String sql = "delete from campaign_item_table where campaign_item_id = ?";
		jdbcTemplate.update(sql,campaignItemId);

	}
	
	
	
	
	
	
}
