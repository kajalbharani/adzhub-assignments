package com.cybage.AdzHubJDBC.model;

public class Creative {
	private int creativeId;

	private String creativeName;

	private String type;

	private int campaignItemId;

	public int getCreativeId() {
		return creativeId;
	}

	public void setCreativeId(int creativeId) {
		this.creativeId = creativeId;
	}

	public String getCreativeName() {
		return creativeName;
	}

	public void setCreativeName(String creativeName) {
		this.creativeName = creativeName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getCampaignItemId() {
		return campaignItemId;
	}

	public void setCampaignItemId(int campaignItemId) {
		this.campaignItemId = campaignItemId;
	}

	@Override
	public String toString() {
		return "Creative [creativeId=" + creativeId + ", creativeName=" + creativeName + ", type=" + type
				+ ", campaignItemId=" + campaignItemId + "]";
	}

}
