package com.cybage.AdzHubJDBC;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdzHubJdbcApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdzHubJdbcApplication.class, args);
	}

}

