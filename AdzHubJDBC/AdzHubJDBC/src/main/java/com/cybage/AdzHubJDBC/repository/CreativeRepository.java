package com.cybage.AdzHubJDBC.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cybage.AdzHubJDBC.model.Creative;
import com.cybage.AdzHubJDBC.model.CreativeRowMapper;

@Transactional

@Repository
public class CreativeRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;

	public int addCreative(Creative creative) {

		String sql = "Insert into creative_table(creative_id,creative_name,type,campaign_item_id) values(?,?,?,?)";
		return jdbcTemplate.update(sql, creative.getCreativeId(), creative.getCreativeName(), creative.getType(),
				creative.getCampaignItemId());
	}

	public Creative getCreativeById(int creativeId) {
		String sql = "Select * from creative_table where creative_id=?";
		RowMapper<Creative> rowMapper = new CreativeRowMapper();
		return jdbcTemplate.queryForObject(sql, rowMapper, creativeId);

	}

	public List<Creative> getAllCreatives() {
		String sql = "Select * from creative_table";
		RowMapper<Creative> rowMapper = new CreativeRowMapper();
		return jdbcTemplate.query(sql, rowMapper);

	}

	public void deleteCreative(int creativeId) {
		String sql = "delete from creative_table where creative_id = ?";
		jdbcTemplate.update(sql, creativeId);

	}

	// updation

	public int updateCreative(List<Creative> creativeList) {
		int success = 0;
		System.out.println("in update");
		String sql = "update creative_table set creative_name=?, type=?, campaign_item_id=? where creative_id=?;";
		for (Creative c : creativeList) {
			System.out.println(c.getCreativeName());
			success = jdbcTemplate.update(sql, c.getCreativeName(), c.getType(), c.getCampaignItemId(),
					c.getCreativeId());
			System.out.println(success);
		}
		return success;

	}

}
