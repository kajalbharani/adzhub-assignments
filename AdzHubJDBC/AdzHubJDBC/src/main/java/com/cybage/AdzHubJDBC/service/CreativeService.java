package com.cybage.AdzHubJDBC.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cybage.AdzHubJDBC.model.Creative;
import com.cybage.AdzHubJDBC.repository.CreativeRepository;


@Service
public class CreativeService {
	
	@Autowired
	CreativeRepository creativeRepo;

	public Creative findById(int creativeId) {
		// TODO Auto-generated method stub
		return  creativeRepo.getCreativeById(creativeId) ;
	}

	

	public int createCreative(Creative creative) {
		// TODO Auto-generated method stub
		return creativeRepo.addCreative(creative);
	}

	public void deleteCreative(int creativeId) {
		// TODO Auto-generated method stub
		creativeRepo.deleteCreative(creativeId);
		
	}

	public Collection<Creative> findAllCreatives() {
		// TODO Auto-generated method stub
		return creativeRepo.getAllCreatives();
	}



	



	public int updateCreative(List<Creative> creativeList) {
		System.out.println("in update service");
//		List<Creative> updatedList= new ArrayList<>();
//		for(Creative c: creativeList)
//		{
//			Creative saved= creativeRepo.getCreativeById(c.getCreativeId());
//			updatedList.add(saved);
//		}
		return creativeRepo.updateCreative(creativeList);
	}

}
