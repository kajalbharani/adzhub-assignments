package com.cybage.AdzHubJDBC.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class CampaignItemRowMapper implements RowMapper<CampaignItem> {

	@Override
	public CampaignItem mapRow(ResultSet rs, int rowNum) throws SQLException {
		CampaignItem campaignItem= new CampaignItem();
		campaignItem.setCampaignItemId(rs.getInt("campaign_item_id"));
		campaignItem.setCampaignName(rs.getString("campaign_name"));
		campaignItem.setCampaignDsp(rs.getString("campaign_dsp"));
		return campaignItem;
	}

}
