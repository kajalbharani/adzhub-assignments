package com.cybage.AdzHubJDBC.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cybage.AdzHubJDBC.exception.CampaignItemNotFoundException;
import com.cybage.AdzHubJDBC.exception.DSPNotAllowedException;
import com.cybage.AdzHubJDBC.model.CampaignItem;


import com.cybage.AdzHubJDBC.service.CampaignItemService;




	@RestController
	@RequestMapping("/campaign")
	public class CampaignItemController {

		@Autowired
		CampaignItemService campaignItemService;

		@GetMapping(path = { "/get/{campaignItemId}" })
		public CampaignItem findCampaignById(@PathVariable("campaignItemId") int campaignItemId) throws Exception {
			return campaignItemService.findById(campaignItemId);
		}

		@GetMapping("/getAll")
		public Collection<CampaignItem> getCampaignItem() {
			return campaignItemService.findAllCampaignItems();

		}

		@PostMapping(path = { "/save" })
		public int createCampaignItem(@RequestBody CampaignItem campaignItem) throws DSPNotAllowedException {
			String campaignDsp = campaignItem.getCampaignDsp();
			
			System.out.println(campaignDsp);

			
			switch(campaignDsp) {
			case "AppNexus":
				campaignItem.setCampaignDsp("AppNexus");
				break;
			case "MediaMath":
				campaignItem.setCampaignDsp("MediaMath");
				break;
			case "DCP":
				campaignItem.setCampaignDsp("DCP");
				break;
			default:
				throw new DSPNotAllowedException(campaignDsp + " not allowed. Allowed DSP values:MediaMAth/AppNexus/DCP");	
			}
			
			return campaignItemService.createCampaignItem(campaignItem);
		}

		@DeleteMapping(path = { "/delete/{campaignItemId}" })
		public void deleteCampaignItem(@PathVariable("campaignItemId") int campaignItemId)
				throws CampaignItemNotFoundException {
			campaignItemService.deleteCampaignItem(campaignItemId);
		}

	}


