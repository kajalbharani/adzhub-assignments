package com.cybage.AdzHubJDBC.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class CreativeRowMapper implements RowMapper<Creative> {

	@Override
	public Creative mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Creative creative= new Creative();
		creative.setCreativeId(rs.getInt("creative_id"));
		creative.setCreativeName(rs.getString("creative_name"));
		creative.setType(rs.getString("type"));
		creative.setCampaignItemId(rs.getInt("campaign_item_id"));
		return creative;
	}

}
