package com.cybage.AdzHubJDBC.model;

import java.util.ArrayList;
import java.util.List;




public class CampaignItem {
	
	public CampaignItem() {
		super();
	}
	
	public CampaignItem(int campaignItemId, String campaignName, String campaign_dsp) {
		super();
		this.campaignItemId = campaignItemId;
		this.campaignName = campaignName;
		this.campaignDsp = campaignDsp;
		
	}

	
	private int campaignItemId;
	

	private String campaignName;
	
	
	private String campaignDsp;
	
	
	

	public int getCampaignItemId() {
		return campaignItemId;
	}


	public void setCampaignItemId(int campaignItemId) {
		this.campaignItemId = campaignItemId;
	}


	public String getCampaignName() {
		return campaignName;
	}


	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}


	public String getCampaignDsp() {
		return campaignDsp;
	}


	public void setCampaignDsp(String campaignDsp) {
		this.campaignDsp = campaignDsp;
	}


	


	


	@Override
	public String toString() {
		return "CampaignItem [campaignItemId=" + campaignItemId + ", campaignName=" + campaignName + ", campaign_dsp="
				+ campaignDsp + "]";
	}

	
	

}

