package com.cybage.AdzHubJDBC.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cybage.AdzHubJDBC.model.CampaignItem;
import com.cybage.AdzHubJDBC.repository.CampaignItemRepository;

@Service
public class CampaignItemService {
	@Autowired
	CampaignItemRepository campaignItemRepo;

	// getById
	public CampaignItem findById(int campaignItemId) {
		return campaignItemRepo.getCampaignItemById(campaignItemId);
	}

	// getAll

	public List<CampaignItem> findAllCampaignItems() {
		return campaignItemRepo.getAllCampaigns();
	}

	// saveItem
	public int createCampaignItem(CampaignItem campaignItem) {
		return campaignItemRepo.addcampaignItem(campaignItem);
	}

	
	//deleteItem
	public void deleteCampaignItem(int campaignItemId) {
		campaignItemRepo.deleteCampaignItem(campaignItemId);
	}
}
