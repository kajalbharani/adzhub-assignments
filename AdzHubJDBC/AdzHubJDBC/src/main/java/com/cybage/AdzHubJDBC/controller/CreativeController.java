package com.cybage.AdzHubJDBC.controller;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cybage.AdzHubJDBC.model.Creative;
import com.cybage.AdzHubJDBC.service.CreativeService;



@RestController
@RequestMapping("/creative")
public class CreativeController {

	@Autowired
	CreativeService creativeService;

	@GetMapping(path = { "/get/{creativeId}" })
	public Creative findCreativeById(@PathVariable("creativeId") int creativeId) throws Exception {
		return creativeService.findById(creativeId);
	}

	@GetMapping("/getAll")
	public Collection<Creative> getCreative() {
		return creativeService.findAllCreatives();

	}

	@PostMapping(path = { "/save" })
	public int createCreative(@RequestBody Creative creative)  {
		
		return creativeService.createCreative(creative);
	}

	@DeleteMapping(path = { "/delete/{creativeId}" })
	public void deleteCreative(@PathVariable("creativeId") int creativeId){
			
		creativeService.deleteCreative(creativeId);
	}

	@PutMapping(path="/update")
	public int updateCreative(@RequestBody List<Creative> creativeList){
		return creativeService.updateCreative(creativeList);
	}
}



